<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisterCont extends Controller
{
    public function register(){
        $hasil = DB::table('karyawan')->get();
        return view('/register',['user'=>$hasil]);
    }

    public function prosesSimpan(Request $req){
        $nama = $req->nama_karyawan;
        $nokaryawan = $req->no_karyawan;
        $notelepon = $req->no_telp_karyawan;
        $jabatan = $req->jabatan_karyawan;
        $divisi = $req->divisi_karyawan;

        DB::table('karyawan')->insert(
            ['nama_karyawan'=>$nama,
            'no_karyawan'=>$nokaryawan, 
            'no_telp_karyawan'=>$notelepon,
            'jabatan_karyawan'=>$jabatan,
            'divisi_karyawan'=>$divisi]
        );

        return redirect('/register');
    }

    public function delete($id){
        DB::table('karyawan')->where('id', $id)->delete();
        return redirect('/register');
    }

    public function edit($id)
    {
        $user = DB::table('karyawan')->where('id', $id)->get();
        return view('/edit', ['user' => $user]);
    }

    public function update (Request $req,Int $id)
    {
        DB::table('karyawan')->where('id', $req->id)->update
        (['nama_karyawan' => $req->nama_karyawan,
        'no_karyawan' => $req->no_karyawan,
        'no_telp_karyawan' => $req->no_telp_karyawan,
        'jabatan_karyawan' => $req->jabatan_karyawan,
        'divisi_karyawan' => $req->divisi_karyawan,]);

        return redirect('/register');
    }
}




