<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <title>Hello, world!</title>
  </head>
  <body>
    
        <div class="container">    
            <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
                <div class="panel panel-info" >
                        <div class="panel-heading">
                            <div class="panel-title">Input data </div>
                        </div>     
    
                        <div style="padding-top:30px" class="panel-body" >
    
                            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                                
                            <form id="loginform" class="form-horizontal" role="form" action="/simpan" method="POST">
                                {{ csrf_field() }}
                                        
                                        <div style="margin-bottom: 25px" class="input-group">
                                          
                                            <input id="login-username" type="text" class="form-control" name="nama_karyawan" value="" placeholder="nama keryawan">                                        
                                        </div>
                                    
                                         <div style="margin-bottom: 25px" class="input-group">
                                    
                                            <input id="login-password" type="number" class="form-control" name="no_karyawan" placeholder="nomor karyawan">
                                        </div>

                                        <div style="margin-bottom: 25px" class="input-group">
                                        
                                            <input id="login-password" type="number" class="form-control" name="no_telp_karyawan" placeholder="nomor telepon">
                                        </div>

                                        <div style="margin-bottom: 25px" class="input-group">
                                      
                                            <input id="login-password" type="text" class="form-control" name="jabatan_karyawan" placeholder="jabatan">
                                        </div>

                                        <div style="margin-bottom: 25px" class="input-group">
                                       
                                            <input id="login-password" type="text" class="form-control" name="divisi_karyawan" placeholder="divisi">
                                        </div>
    
                                    <div style="margin-top:10px" class="form-group">
                                        <!-- Button -->
    
                                        <div class="col-sm-12 controls">
                                            <button type="submit" class="btn btn-primary">Tambah</button>
    
                                        </div>
                                    </div>

                                    </div>    
                                </form>     

                            </div>                     
                        </div>  
            </div>

            <div class="container" style="margin-top:50px;">
                <?php $i=1;?>
              <div class="align-self-center " >
                  <table class="table table-primary table-bordered table-striped">
                      <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">No Karyawan</th>
                            <th scope="col">No Telepon</th>
                            <th scope="col">Jabatan</th>
                            <th scope="col">Divisi</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                    @foreach ($user as $a)
                      <tbody>
                        <tr>
                          <td><?php echo($i); $i++; ?></td>
                          <th scope="col">{{ $a->nama_karyawan }}</th>
                          <th scope="col">{{ $a->no_karyawan }}</th>
                          <th scope="col">{{ $a->no_telp_karyawan }}</th>
                          <th scope="col">{{ $a->jabatan_karyawan }}</th>
                          <th scope="col">{{ $a->divisi_karyawan }}</th>
                          <th scope="col">
                            <a href="/edit/{{ $a->id }}"><button type="button" class="btn btn-success ">Edit</button></a>
                            <a href="/delete/{{ $a->id }}"><button type="button" class="btn btn-danger ">Delete</button></a>
                        </tr>
                      </tbody>
                      @endforeach
                    </table>
              </div>
              </div>

        </div>
        
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  </body>
</html>