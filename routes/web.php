<?php

use App\Http\Controllers\RegisterCont;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register', [RegisterCont::class,'register']);

Route::post('/simpan', [RegisterCont::class,'prosesSimpan']);

Route::get('/delete/{id}',[RegisterCont::class,'delete']);

Route::get('/edit/{id}',[RegisterCont::class,'edit']);

Route::post('/update/{id}',[RegisterCont::class,'update']);

